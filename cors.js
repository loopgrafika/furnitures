// cors.js
module.exports = (req, res, next) => {
  req.header('Access-Control-Allow-Origin', 'http://localhost:3001')
  next()
}