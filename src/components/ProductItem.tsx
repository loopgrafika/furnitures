import { memo, ReactElement } from "react";
import type { Product } from "./App";

export interface ProductItemProps {
  product: Product;
}

function ProductItem(props: ProductItemProps): ReactElement {
  const { product } = props;
  return (
    <div className={`product ${product.id}`} style={{ backgroundColor: product.bgColor }}>
      <img src={product.image} alt={product.name} />
      <p>{product.name}<br /><span className="product-price">{`£${product.price}`}</span></p>
    </div>
  )
}

export default memo(ProductItem);