export { default as App } from './App';
export { default as ProductItem } from './ProductItem';
export { default as ProductCategory } from './ProductCategory';
export type { Product, Furniture } from './App';
