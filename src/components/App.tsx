import React, { ReactElement, useEffect, useState } from 'react';
import axios, { AxiosResponse } from 'axios';
import { BrowserRouter as Router, Route, NavLink, Redirect } from 'react-router-dom';
import { ProductCategory } from '.';

export interface Product {
  id: string
  name: string;
  image: string;
  price: number;
  bgColor: string
}
interface ServerData {
  furniture: Furniture
}
export type Furniture = {
  [key: string]: Product[];
}

function App(): ReactElement {
  const [furniture, setFurniture] = useState<Furniture | null>(null)
  
  useEffect(() => {
    async function fetchData(): Promise<void> {
      const response: AxiosResponse<ServerData> = await axios.get('api');
      
      setFurniture(response.data.furniture);      
    }
    fetchData();
  }, [])

  const navLinks = [
    { label: "Chairs", route: "chairs"},
    { label: "Sofas", route: "sofas"},
    { label: "Tables", route: "tables"},
    { label: "Lamps", route: "lamps"},
    { label: "Kits", route: "kits"}
  ]

  return (
    <div className="main">
      <h1 className="heading">What item are you looking for?</h1>
      <Router>
        <nav className="navigation">
          <ul>
            {navLinks.map(route => <li><NavLink to={`/${route.route}`}>{route.label}</NavLink></li>)}
          </ul>
        </nav>
        <div className="product-list-wrapper">
          <Redirect from="/" to={`/${navLinks[0].route}`} />
          {navLinks.map(route => (
            <Route exact path={`/${route.route}`}>
              {({ match }) => (
                <ProductCategory key={route.label} products={furniture?.[route.route]} category={route.route} match={match} />
              )}
            </Route>
          ))}
        </div>
      </Router>
    </div>
  );
}

export default App;
