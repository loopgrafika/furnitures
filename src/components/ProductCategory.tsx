import { memo } from "react";
import { CSSTransition } from 'react-transition-group';
import type { Product } from "./App";
import { ProductItem } from "./";

export interface ProductCategoryProps {
  products?: Product[];
  category: string;
  match: object | null;
}

function ProductCategory(props: ProductCategoryProps) {
  const { products, category, match } = props;
  return (
    <CSSTransition
      in={match !== null}
      timeout={300}
      classNames="product-list"
      unmountOnExit
    >
      <div className={`product-list ${category}`}>
        <div className="product-wrapper">
          {products?.map((product: Product) => <ProductItem key={product.id} product={product} />)}
        </div>
      </div>
    </CSSTransition>
  )
}

export default memo(ProductCategory);