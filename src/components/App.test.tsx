import { render } from '@testing-library/react';
import App from './App';

test('renders the page without the furniture pieces', () => {
  const { getByText } = render(<App />);
  getByText('What item are you looking for?');

  getByText('Chairs');
  getByText('Sofas');
});
