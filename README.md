# Code Worldwide technical test

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Run the demo

Install it with

`yarn install`.

Spin up the the json-server first:

`json-server --watch db.json --port 3001`

Then run the front-end:

`yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

`yarn test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
